ADDED: MockRuntime::debug_dump() and .as_debug_dump()
DEPRECATED: MockRuntime.advance() 
ADDED: MockRuntime.advance_until(), .advance_by(), .advance_until_blocked()
